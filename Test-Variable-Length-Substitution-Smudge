'''
    @FileName:Test-Variable-Length-Substitution+
    @Author:Keith Watson+
    @LastChangedDate:09-Apr-2016 20:18:58+
    @Revision:2.1.0+
================================================================================
File containing examples of different comment formats used to test the
keyword substitution python module using the default set of delimiters.
'''

+ INLINE COMMENTS
+ ===============
+ Inline comments are generally those that use a newline character to indicate
+ the end of a comment, and an arbitrary delimiter or sequence of tokens to
+ indicate the beginning of a comment.

+-------------------------------------------------------------------------------
+ Ada, AppleScript, Eiffel, Euphoria, Haskell, Lua, Occam, PL/SQL, TSQL, SPARK,
+ ANSI SQL, VHDL

-- @LastChangedDate:09-Apr-2016 20:18:58+

+-------------------------------------------------------------------------------
+ Assembly, AutoHotkey, Lisp, Scheme

; @Date:09-Apr-2016 20:18:58+

+-------------------------------------------------------------------------------
+ AWK, Cobra, Maple, Perl, PHP, Python, Ruby, R, Seed7, Tcl, Windows PowerShell

# @LastChangedRevision:2.1.0+

+-------------------------------------------------------------------------------
+ BASIC, Visual BASIC, .NET, Xojo

' @Revision:2.1.0+

REM @Rev:2.1.0+

+-------------------------------------------------------------------------------
+ C#, D, Java, Go, ECMAScript (JavaScript, ActionScript, etc.),
+ Object Pascal (Delphi), PHP, Rust, Xojo

// @LastChangedBy:Keith Watson+

+-------------------------------------------------------------------------------
+ Erlang, Matlab, S-Lang, Visual Prolog

% @Author:Keith Watson+

+ BLOCK COMMENTS
+ ==============
+ Block comments are generally those that use a delimiter to indicate the
+ beginning of a comment, and another delimiter to indicate the end of a
+ comment. In this context, whitespace and newline characters are not counted
+ as delimiters. NB block comments do not neccessarily have to span more than
+ one line so eack block comment type is repeated; a single line and a multiple
+ line version

+-------------------------------------------------------------------------------
+ Algol 60, Simula

comment @HeadURL:/home/swillber/Projects/Python/GitKeywordFilter/Test-Variable-Length-Substitution+ ;

comment
    @URL:/home/swillber/Projects/Python/GitKeywordFilter/Test-Variable-Length-Substitution+
;

+-------------------------------------------------------------------------------
+ AppleScript, Maple, Mathematica, Object Pascal (Delphi), OCaml, Pascal, Oberon,
+ ML:, Seed7, Standard ML, Wolfram

(* @Id:Test-Variable-Length-Substitution 2.1.0 09-Apr-2016 20:18:58 Keith Watson+ *)

(*
    @FileName:Test-Variable-Length-Substitution+
*)

+-------------------------------------------------------------------------------
+ HTML, XML

<!-- @Id:Test-Variable-Length-Substitution 2.1.0 09-Apr-2016 20:18:58 Keith Watson+ -->

<!--
    @FileName:Test-Variable-Length-Substitution+
-->

+-------------------------------------------------------------------------------
+ AutoHotkey, C, C#, CHILL, PL/I, REXX, Go, ECMAScript (JavaScript, ActionScript,
+ etc.), D, Java, PHP, PL/SQL, TSQL, Visual Prolog, Rust, SAS

/* @AuthorName:Keith Watson+ */

/*
    @AuthorEmail:swillber@gmail.com+
*/

+-------------------------------------------------------------------------------
+ C#, D, Java, Rust (XML / doc comment)

/** @AuthorDate:09-Apr-2016 20:18:58+ */

/**
    @CommitterName:Keith Watson+
*/

+-------------------------------------------------------------------------------
+ AWK, R, Tcl, and Windows PowerShell

<# @CommitterEmail:swillber@gmail.com+ #>

<#
    @CommitterDate:09-Apr-2016 20:18:58+
#>

+-------------------------------------------------------------------------------
+ Object Pascal (Delphi)

{ @RefNames: (HEAD -> master)+ }

{
    @Subject:Testing+
}

+-------------------------------------------------------------------------------
+ Ruby

=begin @RawBody:Testing+ =end

=begin
    @LastChangedDate:09-Apr-2016 20:18:58+
=end

+-------------------------------------------------------------------------------
+ Python

''' @Date:09-Apr-2016 20:18:58+ '''

'''
    @LastChangedRevision:2.1.0+
'''

""" @Revision:2.1.0+ """

"""
    @Rev:2.1.0+
"""


+ LOG HISTORY (COMMIT COMMENTS)
+ =============================

// @LogHistory:..
//         Keith Watson 09-Apr-2016 20:18:58 Testing
//         Keith Watson 09-Apr-2016 20:15:27 Testing
//         Keith Watson 07-Apr-2016 22:23:16 test clean for variable length
//         Keith Watson 07-Apr-2016 21:58:51 Test clean function
//         Keith Watson 06-Apr-2016 22:01:50 Fix log line spacing and remove unneeded quotes
//         Keith Watson 06-Apr-2016 21:59:42 Fix FileName typo and add spacing between lines
//         Keith Watson 06-Apr-2016 19:11:44 Revise git call functions and add trace functions
//         Keith Watson 01-Apr-2016 22:11:07 General Updates
//         Keith Watson 01-Apr-2016 22:02:53 Testing Files Restructured
//         Keith Watson 30-Mar-2016 22:00:16 Testing III
//         Keith Watson 30-Mar-2016 21:57:57 Testing II
//         Keith Watson 30-Mar-2016 21:54:54 Test log history message
//         Keith Watson 30-Mar-2016 21:39:37 Testing
//         Keith Watson 29-Mar-2016 22:02:11 Test clean filter
//         Keith Watson 29-Mar-2016 22:01:36 Fix bugs in keywordsubstitution.py
//         Keith Watson 28-Mar-2016 19:49:57 Add separator to header text
//         Keith Watson 28-Mar-2016 14:25:21 Added version to header
//         Keith Watson 28-Mar-2016 14:24:31 Added date modified to header text.
//         Keith Watson 28-Mar-2016 14:23:18 Added author to header
//         Keith Watson 28-Mar-2016 14:21:12 Added filename to header text.
//         Keith Watson 28-Mar-2016 14:18:02 Expanded header text explanation.
//         Keith Watson 28-Mar-2016 13:59:39 Initial commit
// +

/* @CommitLog:.. */
/*         Keith Watson 09-Apr-2016 20:18:58 Testing */
/*         Keith Watson 09-Apr-2016 20:15:27 Testing */
/*         Keith Watson 07-Apr-2016 22:23:16 test clean for variable length */
/*         Keith Watson 07-Apr-2016 21:58:51 Test clean function */
/*         Keith Watson 06-Apr-2016 22:01:50 Fix log line spacing and remove unneeded quotes */
/*         Keith Watson 06-Apr-2016 21:59:42 Fix FileName typo and add spacing between lines */
/*         Keith Watson 06-Apr-2016 19:11:44 Revise git call functions and add trace functions */
/*         Keith Watson 01-Apr-2016 22:11:07 General Updates */
/*         Keith Watson 01-Apr-2016 22:02:53 Testing Files Restructured */
/*         Keith Watson 30-Mar-2016 22:00:16 Testing III */
/*         Keith Watson 30-Mar-2016 21:57:57 Testing II */
/*         Keith Watson 30-Mar-2016 21:54:54 Test log history message */
/*         Keith Watson 30-Mar-2016 21:39:37 Testing */
/*         Keith Watson 29-Mar-2016 22:02:11 Test clean filter */
/*         Keith Watson 29-Mar-2016 22:01:36 Fix bugs in keywordsubstitution.py */
/*         Keith Watson 28-Mar-2016 19:49:57 Add separator to header text */
/*         Keith Watson 28-Mar-2016 14:25:21 Added version to header */
/*         Keith Watson 28-Mar-2016 14:24:31 Added date modified to header text. */
/*         Keith Watson 28-Mar-2016 14:23:18 Added author to header */
/*         Keith Watson 28-Mar-2016 14:21:12 Added filename to header text. */
/*         Keith Watson 28-Mar-2016 14:18:02 Expanded header text explanation. */
/*         Keith Watson 28-Mar-2016 13:59:39 Initial commit */
/* + */

<!--
    @Log:..
            Keith Watson 09-Apr-2016 20:18:58 Testing
            Keith Watson 09-Apr-2016 20:15:27 Testing
            Keith Watson 07-Apr-2016 22:23:16 test clean for variable length
            Keith Watson 07-Apr-2016 21:58:51 Test clean function
            Keith Watson 06-Apr-2016 22:01:50 Fix log line spacing and remove unneeded quotes
            Keith Watson 06-Apr-2016 21:59:42 Fix FileName typo and add spacing between lines
            Keith Watson 06-Apr-2016 19:11:44 Revise git call functions and add trace functions
            Keith Watson 01-Apr-2016 22:11:07 General Updates
            Keith Watson 01-Apr-2016 22:02:53 Testing Files Restructured
            Keith Watson 30-Mar-2016 22:00:16 Testing III
            Keith Watson 30-Mar-2016 21:57:57 Testing II
            Keith Watson 30-Mar-2016 21:54:54 Test log history message
            Keith Watson 30-Mar-2016 21:39:37 Testing
            Keith Watson 29-Mar-2016 22:02:11 Test clean filter
            Keith Watson 29-Mar-2016 22:01:36 Fix bugs in keywordsubstitution.py
            Keith Watson 28-Mar-2016 19:49:57 Add separator to header text
            Keith Watson 28-Mar-2016 14:25:21 Added version to header
            Keith Watson 28-Mar-2016 14:24:31 Added date modified to header text.
            Keith Watson 28-Mar-2016 14:23:18 Added author to header
            Keith Watson 28-Mar-2016 14:21:12 Added filename to header text.
            Keith Watson 28-Mar-2016 14:18:02 Expanded header text explanation.
            Keith Watson 28-Mar-2016 13:59:39 Initial commit
    +
-->

# @History:..
#         Keith Watson 09-Apr-2016 20:18:58 Testing
#         Keith Watson 09-Apr-2016 20:15:27 Testing
#         Keith Watson 07-Apr-2016 22:23:16 test clean for variable length
#         Keith Watson 07-Apr-2016 21:58:51 Test clean function
#         Keith Watson 06-Apr-2016 22:01:50 Fix log line spacing and remove unneeded quotes
#         Keith Watson 06-Apr-2016 21:59:42 Fix FileName typo and add spacing between lines
#         Keith Watson 06-Apr-2016 19:11:44 Revise git call functions and add trace functions
#         Keith Watson 01-Apr-2016 22:11:07 General Updates
#         Keith Watson 01-Apr-2016 22:02:53 Testing Files Restructured
#         Keith Watson 30-Mar-2016 22:00:16 Testing III
#         Keith Watson 30-Mar-2016 21:57:57 Testing II
#         Keith Watson 30-Mar-2016 21:54:54 Test log history message
#         Keith Watson 30-Mar-2016 21:39:37 Testing
#         Keith Watson 29-Mar-2016 22:02:11 Test clean filter
#         Keith Watson 29-Mar-2016 22:01:36 Fix bugs in keywordsubstitution.py
#         Keith Watson 28-Mar-2016 19:49:57 Add separator to header text
#         Keith Watson 28-Mar-2016 14:25:21 Added version to header
#         Keith Watson 28-Mar-2016 14:24:31 Added date modified to header text.
#         Keith Watson 28-Mar-2016 14:23:18 Added author to header
#         Keith Watson 28-Mar-2016 14:21:12 Added filename to header text.
#         Keith Watson 28-Mar-2016 14:18:02 Expanded header text explanation.
#         Keith Watson 28-Mar-2016 13:59:39 Initial commit
# +
