#!/usr/bin/env python3
"""
Filename: keywordsubstitution.py
Author: Keith Watson
DateCreated: 7/12/2015
DateLastModified: 28/03/2016
PythonVersion: 3.5.1
This file is part of GitKeywordFilter.
A GIT smudge/clean filter implemented in Python to provide for keyword expansion
on checkout. See READ.ME for further details.
"""
#
#===============================================================================
#
# Copyright (C) 2015 Keith Watson swillber@gmail.com
#
#  This file is part of the git keyword package and is free software you can
#  redistribute it and/or modify it under the terms of the GNU Library General
#  Public License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Library General Public License for more details.
#
#  You should have received a copy of the GNU Library General Public License
#  along with this library; see the file LICENSE.txt.  If not, write to
#  the Free Software Foundation, InC, 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#===============================================================================
#
from os import path
import re
import sys
import time
from linecache import getline
from email.utils import parsedate
from datetime import datetime
from subprocess import run as Run
from subprocess import PIPE
from subprocess import CalledProcessError
#
#===============================================================================
#                            F U N C T I O N S
#===============================================================================
#
#-------------------------------------------------------------------------------
def fnGetOptions():
    """
    Process command line arguments to determine if this is a smudge or clean
    invication and to extract path and filename of source file.
    Params: none
    Returns: tuple containing smudge/clean and path+filename of source file
    """
    # ensure that two command line arguments are present
    #  sys.argv[0] = script name,
    #  sys.argv[1] = 'smudge'/'clean',
    #  sys.argv[2] = source file name
    if len(sys.argv) != 3:
        raise RuntimeError("{}\n".format(conf("ERRBADARGS")))
    else:
        # ensure that argument 1 is either 'smudge' or 'clean'
        if sys.argv[1] != 'smudge' and sys.argv[1] != 'clean':
            raise RuntimeError(conf("ERRBADOPT").format(sys.argv[1]))
    return (sys.argv[1], path.realpath(sys.argv[2]))
#
#-------------------------------------------------------------------------------
def fnLoadConf():
    """
    Load configuration data into a dictionary (associative array) of constant
    values called 'conf' where the keys are the constant names. For our purposes
    variable names in all upper case are treated as if they are constants,
    although, to be clear, python doesn't have a native constant type.
    Params: none
    Returns: 'constant' dictionary
    """
    conf = {}                       # initialize empty dictionary
    conf("DLMSTRT") = '@'           # keyword start character
    conf("DLMVEND") = '+'           # variable length keyword end character
    conf("DLMLEND") = '<'           # left just fixed length keyword terminator
    conf("DLMREND") = '>'           # right just fixed length keyword terminator
    conf("DLMSEP") = ':'            # keyword/keyword data separator
    conf("OFLOW") = '..'            # set up overflow indicator string
    conf("OFLEN") = len(conf("OFLOW")) # set up overflow indicator string length
    conf("LOGHIST") = 'LogHistory'  # log history keyword text
    # constants used for git command line functions
    conf("GIT") = '/usr/bin/git'    # full path to git command
    conf("GLOG") = 'log'            # git log command
    # git log command line parameters format string
    conf("LOGLINE") = '-1 --format={} --date=rfc -- {}'
    conf("LOGINFO") = '-1 --format={} -- {}'
    conf("LOGLIST") = '-1 --skip={} --pretty=%>|(20)%an-%ai-%s -- {}'
    conf("REVLINE") = '--tags --max-count=1' # rev-list command arguments string
    conf("REVLIST") = 'rev-list'
    conf("TAGS") = '--tags '
    conf("DESCRIBE") = 'describe'
    conf("LOGDATE") = '%Y-%m-%d %H:%M:%S'   # format of date in git log data
    conf("HISTDATE") = '%d-%b-%Y %H:%M:%S'  # format of date in log history line
    # get script name and script path
    conf("SCRIPTNAME") = path.basename(sys.argv[0])
    conf("SCRIPTPATH") = path.dirname(sys.argv[0])
    #  error messages
    conf("ERRBADARGS") = conf("SCRIPTNAME")
    conf("ERRBADARGS") += " - ERROR: expecting two arguments\n"
    conf("ERRBADOPT") = conf("SCRIPTNAME")
    conf("ERRBADOPT") += " - ERROR: expecting 'smudge'"
    conf("ERRBADOPT") += " or 'clean' but found {}\n"
    #
    # Assuming the default delimiters, a regex of this form;
    #       @(.*?)(([:])(.*))?([+]|[<]+|[>]+)
    # will match unexpanded keyword strings of this form;
    #       @Keyword+
    #       @Keyword>...>
    #       @Keyword<...<
    # and expanded keyword strings of this form;
    #       @Keyword:xxxxxxxxxxxxxxxxxxx
    #       @Keyword:xxxxxxxxxx            <
    #       @Keyword:        xxxxxxxxxxxx>
    # and will return five capture groups;
    #       1 = Keyword string (without leading delimiter)
    # (the next three are always present but empty if keyword unexpanded)
    #       2 = expanded keyword separator and data
    #       3 = expanded keyword separator
    #       4 = expanded keyword data
    #       5 = keyword trailing delimiter(s)
    # Construct the regular expression formatted as described above.
    regex = conf("DLMSTRT")+'(.*?)'
    regex += '((['+conf("DLMSEP")+'])(.*))?'
    regex += '(['+conf("DLMVEND")+']|'
    regex += '['+conf("DLMLEND")+']+|'
    regex += '['+conf("DLMREND")+']+)'
    conf("REPARSE") = re.compile(re.escape(regex))

    # for sourceline of form "# @Keyword<<<<<<<<<<<<<<<<<<" (left aligned) use
    # a regular expression like this '^[^<](*[<]*).*$' to extract the
    # '<<<<<<<<<<<' string as a group.
    regexp = r'^[^{0}](*[{0}]*).*$'
    conf("RELJUST") = re.compile(regexp.format(re.escape(conf("DLMLEND"))))
    #
    # for sourceline of form "# @Keyword>>>>>>>>>>>>>>>>>>" (right aligned) use
    # a regular expression like this '^[^>](*[>]*).*$' to extract the
    # '>>>>>>>>>>>' string as a group.
    regexp = r'^[^{0}](*[{0}]*).*$'
    conf("RERJUST") = re.compile(regexp.format(re.escape(conf("DLMREND"))))

    conf("REGEXVAR") = re.compile(r'{}.*{}'.format(re.escape(conf("DLMSTRT")),
                                                   re.escape(conf("DLMVEND"))))
    # e.g. '@.*'
    conf("REGEXST") = re.compile(r'{}.*'.format(re.escape(conf("DLMSTRT"))))
    # e.g. '\:.*'
    conf("REGEXXVAL") = re.compile(r'{}.*'.format(re.escape(conf("DLMSEP"))))
    # e.g. '\:.*\+'
    conf("REGEXVVAL") = re.compile(r'{}.*{}'.format(re.escape(conf("DLMSEP")),
                                                    re.escape(conf("DLMVEND"))))
    # e.g. '\:.*<'
    conf("REGEXLVAL") = re.compile(r'{}.*{}'.format(re.escape(conf("DLMSEP")),
                                                    re.escape(conf("DLMLEND"))))
    # e.g. '\:.*>'
    conf("REGEXRVAL") = re.compile(r'{}.*{}'.format(re.escape(conf("DLMSEP")),
                                                    re.escape(conf("DLMREND"))))
    # get runtype and  sourcefile path+filename from command line arguments
    conf("RUNTYPE"), conf("SOURCEFILE") = fnGetOptions()
    return conf
#
#-------------------------------------------------------------------------------
def fnFillBack(astring, aseparator, aterminator):
    """
    returns the original string with the characters following a separator
    character replaced with the terminator character.

    NB no validation of supplied parameters is undertaken. It is assumed that
       the source string is not blank and that the 2nd and 3rd arguments are
       single characters.
    """
    slen = len(astring)    # get source string length
    sepfound = False       # unset separator found flag
    result = ""            # variable to hold return value
    # process source string one character at a time
    for char in astring:
        # if the separator is found then switch on the flag
        if char == aseparator:
            sepfound = True
        # if separator not found yet then just copy the current character across
        # otherwise copy the terminator across
        if sepfound:
            result += aterminator
        else:
            result += char
    # return the result string (adding back the newline)
    return result+'\n'
#
#-------------------------------------------------------------------------------
def fnLoadKeywords():
    """
    Create a dictionary of valid keywords and load with the revision information
    Params: none
    Returns: keyword and data dictionary
    """
    keywords = {}               # specify keywords as dictionary
    # load dictionary with equivalent subversion keyword list
    keywords['LastChangedDate'] = fnGitLogDate('%cd')
    keywords['Date'] = fnGitLogDate('%cd')
    keywords['LastChangedRevision'] = fnGitLastTag()
    keywords['Revision'] = fnGitLastTag()
    keywords['Rev'] = fnGitLastTag()
    keywords['LastChangedBy'] = fnGitLogInfo('%cn')
    keywords['Author'] = fnGitLogInfo('%cn')
    keywords['HeadURL'] = conf("SOURCEFILE")
    keywords['URL'] = conf("SOURCEFILE")
    keywords['Id'] = (path.basename(conf("SOURCEFILE"))+
                      ' '+fnGitLastTag()+
                      ' '+fnGitLogDate('%cd')+
                      ' '+fnGitLogInfo('%cn'))
    # load dictionary with new git keywords
    keywords['FileName'] = path.basename(conf("SOURCEFILE"))
    keywords['AuthorName'] = fnGitLogInfo('%an')
    keywords['AuthorEmail'] = fnGitLogInfo('%ae')
    keywords['AuthorDate'] = fnGitLogDate('%ad')
    keywords['CommitterName'] = fnGitLogInfo('%cn')
    keywords['CommitterEmail'] = fnGitLogInfo('%ce')
    keywords['CommitterDate'] = fnGitLogDate('%cd')
    keywords['RefNames'] = fnGitLogInfo('%d')
    keywords['Subject'] = fnGitLogInfo('%s')
    keywords['RawBody'] = fnGitLogInfo('%B')
    # commit / log history list keywords keyword text flags log history required
    keywords['LogHistory'] = conf("LOGHIST")
    keywords['CommitLog'] = conf("LOGHIST")
    keywords['Log'] = conf("LOGHIST")
    keywords['History'] = conf("LOGHIST")
    # return function result
    return keywords
#
#-------------------------------------------------------------------------------
def fnFixedWidth(keyword, keytext, terminator, sourceline):
    """
    Replace keyword with keyword plus fixed width left/right aligned
    expanded text.
    Params:
        keyword (str): keyword being processed
        keytext (str): text associated with keyword
        terminator (str): single left/right fixed width keyword text delimiter
        sourceline (str): sourceline to be processed
    Returns:
        sourceline with left/right aligned fixed width keyword data inserted
        but returns original sourceline if any errors occur.
    """
    # assuming default values for delimiters and a sourceline of form
    # "# @Keyword******************", where '*' can be '<' (left aligned) or
    # '>' (right aligned), use a regular expression of this type
    # '^[^<]*([<]*).*$' to extract the fixed width field string as a group.
    regex = r'^[^{0}]*([{0}]*).*$'
    regex = regex.format(re.escape(terminator))
    # Apply regular expression to sourceline
    match = re.search(regex, sourceline)
    # if found format and align keyword text
    if match:
        # create left or right aligned keyword text
        if terminator == conf("DLMREND"):
            # fixed width right justified
            keyreplace = '{:>{}s}'.format(keytext, len(match.group(1)))
        else:
            # fixed width left justified
            keyreplace = '{:<{}s}'.format(keytext, len(match.group(1)))
        # sourceline with field specifiers replaced with formatted string
        return sourceline.replace(match.group(1), keyreplace)
    else:
        return sourceline
#
#------------------------------------------------------------------------------
def fnLogHistory(sourceline):
    """
    replace the keyword with a comment list containing the reformatted log data;
    e.g. $CommitLog:
                    Keith Watson 24-Aug-2015 13:05 Remove test README changes
                    Keith Watson 24-Aug-2015 13:01 Test README changes
                    Keith Watson 24-Aug-2015 12:58 Initial version
                    Keith Watson 24-Aug-2015 12:51 Initial version
        $
    Params: the original source line containing the log history keyword
    Returns: a comment list containing the reformatted log data
    """
    # output initial log line by replacing trailing delimiter with separator
    # and overflow indicator string to preserve comment character(s)
    result = sourceline.replace(conf("DLMVEND"), conf("DLMSEP")+conf("OFLOW"), 1)
    # iterate over revision history array
    for logline in fnGitLogHistory():
        # and extract the individual fields
        name = logline[0:20]
        date = datetime.strptime(logline[21:40], conf("GLOG")DATE).strftime(conf("HISTDATE"))
        note = logline[47:].rstrip()
        # build output data string
        logout = name+' '+date+' '+note
        # output log line by replacing keyword with output string to preserve
        # comment character(s)
        result += conf("REGEXVAR").sub(logout, sourceline, count=1)
    # replace keyword with trailing delimiter
    result += conf("REGEXVAR").sub(conf("DLMVEND"), sourceline, count=1)
    return result
#
#-------------------------------------------------------------------------------
def fnSmudge(match):
    """
    Examine the current sourceline, if the keyword (plus delimiters) is found
    replace it with the keyword plus the corresponding keyword text as
    specified by the delimiters.
    Params: match object: result of applying regex to sourceline
    Returns: revised source line else original source line
    """
    global conf, Keywords
    # get keyword
    keyword = match.group(1)
    # get text associated with keyword
    keytext = Keywords[keyword]
    # retrieve matching terminator(s) from match object
    terminator = match.group(5)
    # perform keyword expansion depending on terminator
    ### variable length keyword expansion
    if terminator == conf("DLMVEND"):
        # log history is a special case
        if keytext == conf("LOGHIST"):
            # expand keyword to formatted list of commit messages
            sourceline = fnLogHistory(sourceline)
        else:
            # build string with keyword plus data and replace keyword with
            # expanded text
            expandedtext = keyword+conf("DLMSEP")+' '+keytext
            sourceline = sourceline.replace(keyword, expandedtext)
    ### fixed width left or right aligned keyword data
    else:
        # replace keyword with keyword plus fixed width left/right aligned
        # expanded text
        sourceline = fnFixedWidth(keyword, keytext, terminator, sourceline)
    # pass back the sourceline (modified or otherwise)
    return sourceline
#
#-------------------------------------------------------------------------------
def fnClean(Keywords):
    """
    Read from stdin one line at a time. If an expanded keyword is found remove
    the keyword text.
    Params: None
    Returns: None
    """
    global conf, Keywords
    # skip to terminator (log history processing)
    skip2term = False
    # read from stdin one line at a time
    for sourceline in sys.stdin:
        # should we skip lines (presumably log lines)?
        if skip2term:
            # yes, then if a line containing the variable length keyword
            # end character is found...
            if conf("DLMVEND") in sourceline:
                # ...reset the skip lines flag
                skip2term = False
            # skip this line, go back to the start of the loop
            continue
        #  no line skipping so iterate through the keyword list
        for keyword in Keywords:
            # test for presence of this keyword in current line
            fullkeyword = conf("DLMSTRT")+keyword+conf("DLMSEP")
            if fullkeyword in sourceline:
                # test for variable length terminator.
                if conf("DLMVEND") in sourceline:
                    # replace everything from the separator up to the
                    # end of line with one instance of the terminator.
                    cleanedsource = conf("REGEXVVAL").sub(conf("DLMVEND"), sourceline)
                # test for fixed length left justified terminator.
                elif conf("DLMLEND") in sourceline:
                    # replace everything from the separator up to the
                    # end of line with copies of the terminator.
                    cleanedsource = fnFillBack(sourceline, conf("DLMSEP"),
                                               conf("DLMLEND"))
                # test for fixed length right justified terminator.
                elif conf("DLMREND") in sourceline:
                    # replace everything from the separator up to the
                    # end of line with copies of the terminator.
                    cleanedsource = fnFillBack(sourceline, conf("DLMSEP"),
                                               conf("DLMREND"))
                # not one of the expected terminators so we assume this
                # is the start of a log history comment block.
                else:
                    # replace everything from and including the
                    # separator up to the end of line with the variable
                    # length data terminator.
                    cleanedsource = conf("REGEXXVAL").sub(conf("DLMVEND"), sourceline)
                    # set flag to skip log lines
                    skip2term = True
                # replace the old source line with the updated
                # (cleaned) line.
                sourceline = cleanedsource
        # output to stdout without adding any extraneous newlines
        sys.stdout.write(sourceline)
        # ensure stdout buffer is cleared (in effect unbuffered output)
        sys.stdout.flush()
#
#-------------------------------------------------------------------------------
#                        G I T   F U N C T I O N S
#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
def fnRunGit(gitcmd, params):
    """
    Run the git command specified by the first argument augmented by any
    further arguments supplied.
    Params:
        gitcmd (str): git command
        params: auxilliary arguments to git command
    Returns: stdout from command
    Raises: RuntimeError with any stderr response if errors
    """
    # build arguments list, first split params into list
    args = params.split()
    # insert git command then git executable at start of list
    args.insert(0, gitcmd)
    args.insert(0, conf("GIT"))
    # get repository path
    repository = path.dirname(conf("SOURCEFILE"))
    # run the git command
    rungit = Run(args, cwd=repository, stdout=PIPE, universal_newlines=True)
    return rungit.stdout.rstrip('\n')
#
#-------------------------------------------------------------------------------
def fnGitLogDate(dateformat):
    """
    Retrieve date information from the git log history. Date is specified by
    the argument (see man git-log PRETTY FORMATS format:<string> - placeholders
    - NOTE: this is not checked for validity).
    Params: date_format (str): specifies the date info to be returned
    Returns (str): date information in format dd-mmm-yyyy hh:mm:ss
    """
                                        # build command arguments as string
    params = conf("GLOG")LINE.format(dateformat, conf("SOURCEFILE"))
    stdoutput = fnRunGit(conf("GLOG"), params)  # run log command to get date data
    # convert the date time string into a datetime object
    datestamp = email.utils.parsedate(stdoutput)
    # get the date/time as formatted string
    result = time.strftime(conf("HISTDATE"), datestamp)
    return result                       # return the formatted date
#
#-------------------------------------------------------------------------------
def fnGitLastTag():
    """
    Output the tag of the latest tagged commit in all branches
    ref: http://stackoverflow.com/questions/1404796/
                        how-to-get-the-latest-tag-name-in-current-branch-in-git
    Params: none
    Returns: the tag of the latest tagged commit in all branches
    """
    # run rev-list command and use output as argument to describe command
    revlist = fnRunGit(conf("REVLIST"), conf("REVLINE"))
    # run describe command to retrieve tag
    result = fnRunGit(conf("DESCRIBE"), conf("TAGS")+revlist)
    return result                       # return tag data
#
#-------------------------------------------------------------------------------
def fnGitLogInfo(specifier):
    """
    Interrogate log for information from the latest revision. Information to be
    retrieved is specified by the argument (see man git-log PRETTY FORMATS
    format:<string> - placeholders).
    Params: specifier (str): define data to be retrieved
    Returns: data from the latest revision
    """
    # build command arguments as string
    params = conf("GLOG")INFO.format(specifier, conf("SOURCEFILE"))
    result = fnRunGit(conf("GLOG"), params)    # run git log command
    return result                      # return log data
#
#-------------------------------------------------------------------------------
def fnGitLogHistory():
    """
    use the git log command to get all the revision history lines as a formatted
    list (author name, author date, commit note subject line) one by one and
    add them in order to the result array;
    e.g.
        Keith Watson 2015-08-24 13:05:14 +0100 Remove test README changes
        Keith Watson 2015-08-24 13:01:59 +0100 Test README changes
        Keith Watson 2015-08-24 12:58:15 +0100 Initial version
        Keith Watson 2015-08-24 12:51:49 +0100 Initial version
    Params: none
    Returns: data from the latest revision
    NB processing is line by line instead of a single log query to avoid
    problems where the results are truncated due to buffer and pseudo-terminal
    line length issues.
    """
    # set up an empty array to hold the result
    result = []
    # variable used to count lines read so far and move each query on to next
    skip = 0
    # read successive commit log lines
    while True:
        # initialize variable used to return log info
        gitlogline = ''
        # set up git log command arguments
        params = conf("GLOG")LIST.format(skip, path.dirname(conf("SOURCEFILE")))
        # get next git commit log line
        gitlogline = fnRunGit(conf("GLOG"), params)
        # test to see if end of list
        if len(gitlogline) > 0:
            # add this reult to result array
            result.append(gitlogline)
            # move on to next entry
            skip += 1
        else:
            # assume an empty result means the last line
            break
    # return log data
    return result
#
#-------------------------------------------------------------------------------
#                   M A I N   L O G I C   F U N C T I O N
#-------------------------------------------------------------------------------
#
# load configuration data
conf = fnLoadConf()
# load Keywords dictionary
Keywords = fnLoadKeywords()
#
#-------------------------------------------------------------------------------
def fnMain():
    """
    script execution primary logic
    """
    try:
        # skip to terminator flag (clean log history processing only)
        skip2term = False
        # read from stdin one line at a time
        for sourceline in sys.stdin:
            # should we skip lines (presumably log lines)?
            if skip2term:
                # yes, then if a line containing the variable length keyword
                # end character is found...
                if conf("DLMVEND") in sourceline:
                    # ...reset the skip lines flag
                    skip2term = False
                # skip this line, go back to the start of the loop
                continue
            # Scan through the sourceline looking for the first location where
            # the compiled regex pattern matches returning a match object.
            match = conf("REPARSE").search(sourceline)
            # Match objects always have a boolean value of 'True' and return
            # 'None' when there is no match.
            if match:
                # see if the first capture group is a valid keyword
                if Keywords.has_key(match.group(1)):
                    # do smudge or clean depending on run type
                    if conf("RUNTYPE") == 'smudge':
                        # use the match object to expand the keyword
                        sourceline = fnSmudge(match)
                    else: # assume conf("RUNTYPE") == 'clean'
                        # examine source removing any keyword data
                        # NB returns tuple of log history flag and sourceline
                        skip2term, sourceline = fnClean(match)
            # output to stdout without adding any extraneous newlines
            # ensure stdout buffer is cleared (in effect unbuffered output)
            print(sourceline, end='', flush=True)
    except (BrokenPipeError, IOError):
        pass
    sys.stderr.close()
#
#===============================================================================
#                T R A C E   C A L L B A C K   F U N C T I O N S
#===============================================================================
#
"""
All Code in this section is based on and shamelessly filched from
http://bioportal.weizmann.ac.il/course/python/PyMOTW/PyMOTW/docs/sys/tracing.html#sys-tracing
(which I highly recommend)

The trace hook is invoked by passing a callback function to sys.settrace().
The callback will receive three arguments;
    1. Frame (the stack frame from the code being run),
    2. event (a string naming the type of notification),
    3. arg (an event-specific value).

There are 7 event types for different levels of information that occur as a
program is being executed. We are only interested in 4 of them.

    Event       When                            arg value
    =====================================================================
    'call'      Before a function is executed.  None
    'line'      Before a line is executed.      None
    'return'    Before a function returns.      The value being returned.
    'exception' After an exception occurs.      The (exception, value,
                                                traceback) tuple.

see https://docs.python.org/3/library/sys.html#sys.settrace for more info.

NB all object references have uppercase first character.
"""
#
#-------------------------------------------------------------------------------
def fnTraceCall(Frame, event, arg):
    """
    A call event is generated before every function call. The frame passed to
    the callback can be used to find out which function is being called and
    from where. (see https://docs.python.org/3/library/inspect.html for
    object - attribute descriptions)

    This function handles the basic call events and will return None (default)
    if no further tracing is required within the call scope or may return a
    reference to another function if tracing within the scope is wanted.
    """
    global tracedepth, conf, Keywords
    # only handle 'call' events
    if event == 'call':
        # get Code object being executed in this Frame
        Code = Frame.f_code
        # get name within which this Code object was defined
        fnname = Code.co_name
        # if entries in list of functions to trace...
        if len(conf("TRACELIST")) > 0:
            # and current function not in list, then return
            if fnname not in conf("TRACELIST"):
                return
        # Ignore write() calls from print statements
        if fnname == 'write':
            return
        # get current line number in source Code
        fnlineno = Frame.f_lineno
        # get sourceline
        fnline = getline(path.basename(sys.argv[0]), fnlineno)
        # get name of file in which this Code object was created
        fnfilename = Code.co_filename
        # get next outer Frame object (this frame’s Caller)
        Caller = Frame.f_back
        # get line number in Caller's source Code
        callerlineno = Caller.f_lineno
        # get sourceline
        callerline = getline(path.basename(sys.argv[0]), callerlineno)
        # get filename where calling function is defined
        callerfilename = Caller.f_code.co_filename
        # calculate prefix used to indicate call depth
        depth = '  ' * tracedepth
        # only report calls from functions in current module to functions in
        # current module
        if conf("SCRIPTNAME") in callerfilename:
            if conf("SCRIPTNAME") in fnfilename:
                # build formatted string
                trace='{}{}    Call {}()[==>{}]'
                traceline=trace.format(callerlineno, depth, fnname, fnlineno)
                # write trace information to trace file
                print(traceline, file=conf("TRACEOUT"), flush=True)
                # increment call stack depth
                tracedepth += 1
                # set callback to trace lines within this function
                return fnTraceLines
    else:
        return
#
#-------------------------------------------------------------------------------
def fnTraceLines(Frame, event, arg):
    """
    Callback used to trace lines within the scope of a function call.
    """
    global tracedepth, conf, Keywords
    # get Code object being executed in this Frame
    Code = Frame.f_code
    # get name within which this Code object was defined
    fnname = Code.co_name
    # if entries in list of functions to trace...
    if len(conf("TRACELIST")) > 0:
        # and current function not in list, then return
        if fnname not in conf("TRACELIST"):
            return
    # get current line number in source Code
    lineno = Frame.f_lineno
    # get sourceline
    statement = getline(conf("SCRIPTNAME"), lineno)
    # calculate prefix used to indicate call depth
    depth = '  ' * tracedepth
    # report line execution event
    if event == 'line':
        # ignore 'return' lines (separate event type)
        if 'return' in statement:
            return
        # build formatted string
        trace = '{}{}{}'
        traceline = trace.format(lineno, depth, statement)
        # write trace information to trace file and supress newline
        # uncomment next line if list of executed statements needed
        # print(traceline, end='', file=conf("TRACEOUT"), flush=True)
    # report function return value
    elif event == 'return':
        # build formatted string
        trace='{}{}  ==<{}>'
        traceline=trace.format(lineno, depth, arg)
        # write trace information to trace file
        print(traceline, file=conf("TRACEOUT"), flush=True)
        # decrement call stack depth
        tracedepth -= 1
    else:
        return
#
#===============================================================================
#                    I N I T I A T E   T R A C E
#===============================================================================
#
# list of functions to trace (trace all if list empty)
conf("TRACELIST") = ['fnLogHistory', 'fnGitLogHistory']
# used to track depth of call stack
tracedepth = 0
# trace file open error message
conf("ERRTRACE") = conf("SCRIPTNAME")+" - ERROR: error opening trace file\n"
# build trace file name
conf("TRACEFILE") = path.splitext(conf("SCRIPTNAME"))[0]+'.trace'
# uncomment the following line to trace filter execution and write output to a
# trace file.
# open trace file for writing
# try:
#     conf("TRACEOUT") = open(conf("TRACEFILE"), mode='w')
# except:
#     raise RuntimeError("{}".format(conf("ERRBADARGS")))
#
# invoke the settrace function via a reference to a callback function. The
# callback function is invoked (with event set to 'call') whenever a function is
# called; it should return a reference to a line trace function to be used for
# that function, or None if the lines are not to be traced.
#
# sys.settrace(fnTraceCall)
#
#===============================================================================
# if being run directly invoke main function
#===============================================================================
#
if __name__ == '__main__':
    fnMain()
